package techorin.com.smsgateway;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import dmax.dialog.SpotsDialog;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import techorin.com.smsgateway.beans.LoginBean;


public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private Button button_login;
    private EditText phone_number, pin_code;
    private SpotsDialog progress_dialog;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_login);

        /* Create Component Instances*/
        button_login = (Button) findViewById(R.id.button_login);
        phone_number = (EditText) findViewById(R.id.phoneNumber);
        pin_code = (EditText) findViewById(R.id.pinCode);
        progress_dialog = new SpotsDialog(this, "Authenticating...");

        Context context = getApplicationContext();
        sharedPreferences = context.getSharedPreferences(
                getString(R.string.preference_file_key), Context.MODE_PRIVATE);
        button_login.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.button_login:
                if (validateCredentials(phone_number.getText(), pin_code.getText())) {
                    if (isOnline()) {
                        doLogin(phone_number.getText(), pin_code.getText());
                    }else{
                        Intent i = new Intent(LoginActivity.this,NoInternetActivity.class);
                        startActivity(i);
                    }
                }
        }
    }

    private boolean validateCredentials(Editable phone_number, Editable pin_code) {
        if (phone_number == null) {
            return false;
        }
        if (pin_code == null) {
            return false;
        }
        return true;
    }

    private void doLogin(Editable phone_number, Editable pin_code) {
        progress_dialog.show();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("mobile", phone_number.toString());
            jsonObject.put("password", pin_code.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }


        OkHttpClient client = new OkHttpClient();

        MediaType JSON = MediaType.parse("application/json; charset=utf-8");
        okhttp3.RequestBody body = RequestBody.create(JSON, jsonObject.toString());


        okhttp3.Request request = new Request.Builder()
                .url(ApplicationContants.BASE_URL + ApplicationContants.LOGIN)
                .post(body)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, final IOException e) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progress_dialog.dismiss();
                        Toast.makeText(LoginActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                try {
                    String responseString = response.body().string();

                    JSONObject jsonObject = new JSONObject(responseString);
                    Gson gson = new Gson();

                    final LoginBean loginBean = gson.fromJson(jsonObject.toString(), LoginBean.class);

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            updateUI(loginBean);
                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void updateUI(LoginBean loginBean) {
        progress_dialog.cancel();
        if (loginBean.getResult()) {
            editor = sharedPreferences.edit();
            editor.putString(getString(R.string.phone_number_preference), phone_number.getText().toString());
            editor.putString(getString(R.string.token), loginBean.getToken().toString());
            editor.commit();
            Intent verifyIntent = new Intent();
            verifyIntent.setClass(getApplicationContext(), VerificationActivity.class);
            Log.d("OTP",loginBean.getToken());
            verifyIntent.putExtra("token", loginBean.getToken());

            startActivity(verifyIntent);
        } else {
            Toast.makeText(getApplicationContext(), "Invalid Login! Please Try Again.", Toast.LENGTH_LONG).show();
        }
    }

    public boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }
}