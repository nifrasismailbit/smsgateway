package techorin.com.smsgateway;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.IBinder;
import android.telephony.SmsMessage;
import android.util.Log;
import android.widget.Toast;

import static android.content.ContentValues.TAG;

public class SmsReceiver extends BroadcastReceiver {

    private SharedPreferences sharedPreferences;
    private Context mContext;
    public SmsReceiver(){

    }
    @Override
    public void onReceive(Context context, Intent intent) {
        mContext = context;
        sharedPreferences = context.getSharedPreferences(
                context.getString(R.string.preference_file_key), Context.MODE_PRIVATE);

        final Bundle bundle = intent.getExtras();
        try {
            if (bundle != null) {
                Object[] pdusObj = (Object[]) bundle.get("pdus");
                for (Object aPdusObj : pdusObj) {
                    SmsMessage currentMessage = SmsMessage.createFromPdu((byte[]) aPdusObj);
                    String senderAddress = currentMessage.getDisplayOriginatingAddress();
                    String message = currentMessage.getDisplayMessageBody();

                    Log.e("OTP", "Received SMS: " + message + ", Sender: " + senderAddress);

                    // verification code from sms
                    String verificationCode = getVerificationCode(message);
                    Log.e("OTP", "OTP received: " + verificationCode);
                    if(isVerified(verificationCode)){
                        Intent dialogIntent = new Intent(context.getApplicationContext(), MainActivity.class);
                        dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(dialogIntent);
                    }else{
                        Toast.makeText(context,"Verification Failed",Toast.LENGTH_LONG).show();
                    }
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e.getMessage());
        }
    }

    private boolean isVerified(String receivedVerificationCode) {
        Boolean isValid = false;
        String sendVerificationCode = sharedPreferences.getString(mContext.getString(R.string.verification_code_preference),"");
        if(receivedVerificationCode.equals(sendVerificationCode)){
            isValid = true;
        }
        return isValid;
    }

    /**
     * Getting the OTP from sms message body
     * ':' is the separator of OTP from the message
     *
     * @param message
     * @return
     */
    private String getVerificationCode(String message) {
        String code = null;
        int index = message.indexOf(":");

        if (index != -1) {
            int start = index + 2;
            int length = 6;
            code = message.substring(start, start + length);
            return code;
        }

        return code;
    }
}
