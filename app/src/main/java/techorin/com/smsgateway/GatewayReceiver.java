package techorin.com.smsgateway;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.preference.PreferenceActivity;
import android.telephony.SmsManager;
import android.text.Editable;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.sql.Time;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import techorin.com.smsgateway.beans.LoginBean;
import techorin.com.smsgateway.beans.Message;
import techorin.com.smsgateway.beans.ResponseBean;

/**
 * Created by nifras on 8/24/17.
 */

public class GatewayReceiver extends BroadcastReceiver {

    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    private Context mContext;
    private String phone_number,token;

    private int MSENT = 1;
    private int MNOT_SENT = 2;
    private int MDELIVERED = 3;
    private int MNOT_DELIVERED = 4;

    @Override
    public void onReceive(Context context, Intent intent) {
        mContext = context;
        sharedPreferences = context.getSharedPreferences(
                context.getString(R.string.preference_file_key), Context.MODE_PRIVATE);
        Log.d("RES123456","Received ");
        phone_number = sharedPreferences.getString("phone_number", "");
        token = sharedPreferences.getString("token", "");
        pullingMessageFromServer();


        //Toast.makeText(context,"Triggered Now " + Calendar.getInstance().getTime(), Toast.LENGTH_LONG).show();
    }

    private void pullingMessageFromServer() {
        Log.d("RES123456","Pull Request Started");
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("mobile", phone_number );
            jsonObject.put("token", token );
        } catch (JSONException e) {
            e.printStackTrace();
        }


        OkHttpClient client = new OkHttpClient();

        MediaType JSON = MediaType.parse("application/json; charset=utf-8");
        okhttp3.RequestBody body = RequestBody.create(JSON, jsonObject.toString());


        okhttp3.Request request = new Request.Builder()
                .url(ApplicationContants.BASE_URL + ApplicationContants.PULL_MESSAGES)
                .addHeader("Authorization","Bearer "+ token)
                .addHeader("Content-Type","application/json")
                .post(body)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, final IOException e) {
                Log.d("RES123456", "Failed to Server");
                //Toast.makeText(mContext,"Failed on Service",Toast.LENGTH_LONG).show();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String responseString = response.body().string();
                Log.d("RES123456", responseString);

                try {
                    JSONObject jsonObject = new JSONObject(responseString);
                    Gson gson = new Gson();

                    final ResponseBean responseBean = gson.fromJson(jsonObject.toString(), ResponseBean.class);
                    if(responseBean.getResult()) {
                        Log.d("RES123456", "Send SMS Request Send");
                        Log.d("RES123456", responseBean.toString());
                        sendSMS(responseBean);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


               // Toast.makeText(mContext,"Received Response : " + Calendar.getInstance().getTime() , Toast.LENGTH_LONG ).show();
            }
        });
    }

    private void sendSMS(ResponseBean responseBean){
        for (Message message : responseBean.getData()) {
            if(message.getUsers() != null)
            sendMessage(message.getUsers().getMobile(),message.getMessage(), message.getId());
        }
    }

    private void sendMessage(String phone_number,String message,String messageId) {

        sendSMS(phone_number,message,messageId);
        Log.d("RES123456","SMS SENDING TO  : " + phone_number + " -  " + messageId);


    }

    private void sendSMS(String  phoneNumber, String  message, final String messageId)
    {
        String  SENT = "SMS_SENT";
        String  DELIVERED = "SMS_DELIVERED";

        PendingIntent sentPI = PendingIntent.getBroadcast(mContext.getApplicationContext(), 0,
                new Intent(SENT), 0);

        PendingIntent deliveredPI = PendingIntent.getBroadcast(mContext.getApplicationContext(), 0,
                new Intent(DELIVERED), 0);

        //---when the SMS has been sent---
        mContext.getApplicationContext().registerReceiver(new BroadcastReceiver(){
            @Override
            public void onReceive(Context  arg0, Intent arg1) {
                    if(getResultCode() == Activity.RESULT_OK){
                        sendStatus(messageId,MSENT);
                        Log.d("RES123456","DELIVERY REPORT : " + MSENT);
                    }else{
                        sendStatus(messageId,MNOT_SENT);
                        Log.d("RES123456","DELIVERY REPORT : " + MNOT_SENT);
                    }
            }
        }, new IntentFilter(SENT));

        //---when the SMS has been delivered---
        mContext.getApplicationContext().registerReceiver(new BroadcastReceiver(){
            @Override
            public void onReceive(Context  arg0, Intent arg1) {
                switch (getResultCode())
                {
                    case Activity.RESULT_OK:
                        sendStatus(messageId,MDELIVERED);
                        Log.d("RES123456","DELIVERY REPORT : " + MDELIVERED);

                        break;
                    case Activity.RESULT_CANCELED:
                        sendStatus(messageId,MNOT_DELIVERED);
                        Log.d("RES123456","DELIVERY REPORT : " + MNOT_DELIVERED);

                        break;
                }
            }
        }, new IntentFilter(DELIVERED));

        SmsManager sms = SmsManager.getDefault();
        sms.sendTextMessage(phoneNumber, null, message, sentPI, deliveredPI);
        deleteSMS(mContext,message,phoneNumber);
    }


    /**
     * Delete a particular message
     * @param context
     * @param message
     * @param number
     */
    public void deleteSMS(Context context, String message, String number) {
        try {
            Uri uriSms = Uri.parse("content://sms/sent");
            Cursor c = context.getContentResolver().query(
                    uriSms,
                    new String[] { "_id", "thread_id", "address", "person",
                            "date", "body" }, "read=0", null, null);

            if (c != null && c.moveToFirst()) {
                do {
                    long id = c.getLong(0);
                    long threadId = c.getLong(1);
                    String address = c.getString(2);
                    String body = c.getString(5);
                    String date = c.getString(3);
                    Log.e("log>>>",
                            "0>" + c.getString(0) + "1>" + c.getString(1)
                                    + "2>" + c.getString(2) + "<-1>"
                                    + c.getString(3) + "4>" + c.getString(4)
                                    + "5>" + c.getString(5));
                    Log.e("log>>>", "date" + c.getString(0));

                    if (message.equals(body) && address.equals(number)) {
                        // mLogger.logInfo("Deleting SMS with id: " + threadId);
                        context.getContentResolver().delete(
                                Uri.parse("content://sms/" + id), "date=?",
                                new String[] { c.getString(4) });
                        Log.e("log>>>", "Delete success.........");
                    }
                } while (c.moveToNext());
            }
        } catch (Exception e) {
            Log.e("log>>>", e.toString());
        }
    }

    private void sendStatus(String messageId, int status){
        Log.d("RES123456","Status Send");
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("id", messageId );
            jsonObject.put("status", status );

        } catch (JSONException e) {
            e.printStackTrace();
        }


        OkHttpClient client = new OkHttpClient();

        MediaType JSON = MediaType.parse("application/json; charset=utf-8");
        okhttp3.RequestBody body = RequestBody.create(JSON, jsonObject.toString());


        okhttp3.Request request = new Request.Builder()
                .url(ApplicationContants.BASE_URL + ApplicationContants.STATUS)
                .addHeader("Authorization","Bearer " + token)
                .addHeader("Content-Type","application/json")
                .post(body)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, final IOException e) {
                Log.d("RES123456", "Failed to Server");
                //Toast.makeText(mContext,"Failed on Service",Toast.LENGTH_LONG).show();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String responseString = response.body().string();
                //Log.d("RES123456", responseString);
                Log.d("RES123456",responseString);
                // Toast.makeText(mContext,"Received Response : " + Calendar.getInstance().getTime() , Toast.LENGTH_LONG ).show();
            }
        });
    }
}
