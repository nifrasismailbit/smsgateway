package techorin.com.smsgateway.beans;

/**
 * Created by nifras on 8/24/17.
 */

public class Message {
        private String id,message,reciever,status;
    private User users;

    public Message(String id, String message, String reciever, String status) {
        this.id = id;
        this.message = message;
        this.reciever = reciever;
        this.status = status;
    }

    public Message(String id, String message, String reciever, String status, User users) {
        this.id = id;
        this.message = message;
        this.reciever = reciever;
        this.status = status;
        this.users = users;
    }

    public User getUsers() {
        return users;
    }

    public void setUsers(User users) {
        this.users = users;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getReciever() {
        return reciever;
    }

    public void setReciever(String reciever) {
        this.reciever = reciever;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
