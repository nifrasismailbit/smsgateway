package techorin.com.smsgateway.beans;

/**
 * Created by nifras on 9/19/17.
 */

public class ReportBean {
    private Boolean result;
    private ReportDataBean data;

    public ReportBean(Boolean result, ReportDataBean data) {
        this.result = result;
        this.data = data;
    }

    public Boolean getResult() {
        return result;
    }

    public void setResult(Boolean result) {
        this.result = result;
    }

    public ReportDataBean getData() {
        return data;
    }

    public void setData(ReportDataBean data) {
        this.data = data;
    }
}
