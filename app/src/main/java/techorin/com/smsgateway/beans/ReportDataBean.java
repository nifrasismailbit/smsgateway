package techorin.com.smsgateway.beans;

/**
 * Created by nifras on 9/19/17.
 */

public class ReportDataBean {
    private int sent,failed,delivered;

    public ReportDataBean(int sent, int failed, int delivered) {
        this.sent = sent;
        this.failed = failed;
        this.delivered = delivered;
    }

    public int getSent() {
        return sent;
    }

    public void setSent(int sent) {
        this.sent = sent;
    }

    public int getFailed() {
        return failed;
    }

    public void setFailed(int failed) {
        this.failed = failed;
    }

    public int getDelivered() {
        return delivered;
    }

    public void setDelivered(int delivered) {
        this.delivered = delivered;
    }
}
