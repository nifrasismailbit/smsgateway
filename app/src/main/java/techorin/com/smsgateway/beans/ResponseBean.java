package techorin.com.smsgateway.beans;

public class ResponseBean {
    private String message;
    private Boolean result;

    public ResponseBean(String message, Boolean result, Message[] data) {
        this.message = message;
        this.result = result;
        this.data = data;
    }

    private Message data[];

    public Message[] getData() {
        return data;
    }

    public void setData(Message[] data) {
        this.data = data;
    }

    public ResponseBean(String message, Boolean result) {
        this.message = message;
        this.result = result;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Boolean getResult() {
        return result;
    }

    public void setResult(Boolean result) {
        this.result = result;
    }
}
