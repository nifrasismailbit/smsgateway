package techorin.com.smsgateway.beans;

public class LoginBean {
    private Boolean result;
    private String token;

    public LoginBean(Boolean result, String token) {
        this.result = result;
        this.token = token;
    }

    public Boolean getResult() {
        return result;
    }

    public void setResult(Boolean result) {
        this.result = result;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
