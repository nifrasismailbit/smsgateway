package techorin.com.smsgateway;

import android.app.ActivityManager;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.SystemClock;
import android.support.v4.app.AlarmManagerCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import techorin.com.smsgateway.beans.LoginBean;
import techorin.com.smsgateway.beans.ReportBean;
import techorin.com.smsgateway.beans.ResponseBean;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    private Button button_start_service;
    private String phone_number,token;
    private TextView countSent, countNotSent,countPending;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().setTitle("Adon SMS Gateway");


        sharedPreferences = getApplicationContext().getSharedPreferences(
                getApplicationContext().getString(R.string.preference_file_key), Context.MODE_PRIVATE);
        phone_number = sharedPreferences.getString("phone_number", "");
        token = sharedPreferences.getString("token", "");

        countSent = (TextView)findViewById(R.id.countSent);
        countNotSent = (TextView)findViewById(R.id.countNotSent);
        countPending = (TextView)findViewById(R.id.countPending);


        button_start_service = (Button)findViewById(R.id.button_start_service);


        if(isOnline()){
            updateReports();
        }else{
            Intent i = new Intent(MainActivity.this,NoInternetActivity.class);
            startActivity(i);
        }
        button_start_service.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.button_start_service:
                startGateway();
                Log.d("RES123456","Start Service Clicked");
        }
    }

    private void updateReports() {
        Log.d("RES123456","Update Report");
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("mobile", phone_number );
            jsonObject.put("token", token );
        } catch (JSONException e) {
            e.printStackTrace();
        }


        OkHttpClient client = new OkHttpClient();

        MediaType JSON = MediaType.parse("application/json; charset=utf-8");
        okhttp3.RequestBody body = RequestBody.create(JSON, jsonObject.toString());


        okhttp3.Request request = new Request.Builder()
                .url(ApplicationContants.BASE_URL + ApplicationContants.REPORT)
                .addHeader("Authorization","Bearer "+ token)
                .addHeader("Content-Type","application/json")
                .post(body)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, final IOException e) {
                Log.d("RES123456", "Failed to Server");
                //Toast.makeText(mContext,"Failed on Service",Toast.LENGTH_LONG).show();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                try {
                    String responseString = response.body().string();
                    Log.d("RES123456", responseString);

                    JSONObject jsonObject = new JSONObject(responseString);
                    Gson gson = new Gson();


                    final ReportBean reportBean = gson.fromJson(jsonObject.toString(), ReportBean.class);

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            updateUI(reportBean);
                        }
                    });
                }catch (JSONException e){
                    e.printStackTrace();
                }
                // Toast.makeText(mContext,"Received Response : " + Calendar.getInstance().getTime() , Toast.LENGTH_LONG ).show();
            }


        });
    }

    private void updateUI(ReportBean reportBean) {
        countSent.setText(reportBean.getData().getSent()+"");
        countNotSent.setText(reportBean.getData().getFailed()+"");
        countPending.setText(reportBean.getData().getDelivered()+"");
    }

    public boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    private void startGateway() {
        AlarmManager alarmManager = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(MainActivity.this,GatewayReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this,0,intent,0);
        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, SystemClock.elapsedRealtime()+3000,60000,pendingIntent);
    }

}
