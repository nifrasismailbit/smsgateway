package techorin.com.smsgateway;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.util.Log;
import android.widget.Toast;

import java.util.Random;

import dmax.dialog.SpotsDialog;

public class VerificationActivity extends AppCompatActivity {

    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    private SpotsDialog progress_dialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_verification);

        Context context = getApplicationContext();
        sharedPreferences = context.getSharedPreferences(
                getString(R.string.preference_file_key), Context.MODE_PRIVATE);

        progress_dialog = new SpotsDialog(this, "Verifying Your Mobile");
        progress_dialog.show();

        String phone_number = sharedPreferences.getString(getString(R.string.phone_number_preference), "");
        sendVerificationCode(phone_number);
    }

    private void sendVerificationCode(String phone_number) {
        Random rnd = new Random();
        int verificationCodeinNumber = 100000 + rnd.nextInt(900000);
        String verificationCode = verificationCodeinNumber+"";
        SmsManager smsManager = SmsManager.getDefault();
        String messageBody = "Your OTP is : "+verificationCode;
        smsManager.sendTextMessage(phone_number, null, messageBody , null, null);
        editor = sharedPreferences.edit();
        editor.putString(getString(R.string.verification_code_preference), verificationCode);
        editor.commit();
    }


}
