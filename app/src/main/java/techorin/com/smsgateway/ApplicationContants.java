package techorin.com.smsgateway;

/**
 * Created by nifras on 8/17/17.
 */

class ApplicationContants {
    public static final String BASE_URL = "http://adon.lk/";
    public static final String LOGIN = "api/login";
    public static final String PULL_MESSAGES = "api/messages";
    public static final String STATUS = "api/status";
    public static final String REPORT = "api/report";

    public static final String SMS_ORIGIN = "ADDON";
    public static final String OTP_DELIMITER = ":";
}
